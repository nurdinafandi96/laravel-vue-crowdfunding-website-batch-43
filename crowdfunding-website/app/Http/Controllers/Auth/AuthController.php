<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\OtpCode;
use Carbon\Carbon;
use App\Events\UserRegisterEvent;


class AuthController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed|min:8',
            'name' => 'required'
        ]);

        $user = User::create([
            'email' => $request['email'],
            'name' => $request['name'],
            'password' => Hash::make($request->password),
        ]);

        $data['user'] = $user;

        event(new UserRegisterEvent($user));

        // return response()->json([
        //     'response_code' => '00',
        //     'response_massage' => 'user berhasil register',
        //     'data' => $user
        // ], 201);

        return response()->json([
            'response_code' => '00',
            'message' => 'User created successfully | user berhasil di register',
            'data' => $data,
        ], 201);
    }

    public function login(Request $request)
    {
        $request->validate([
            //'email' => 'required',
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'email dan password invalid'], 401);
        }

        $data['token'] = $token;

        return response()->json([
            'response_code' => '00',
            'response_massage' => 'user berhasil login',
            'data' => [
                'token' => $token
            ]
        ], 201);

        // if (!Auth::attempt($request->only('email', 'password'))) {
        //     return response()->json([
        //         'response_code' => '401',
        //         'message' => 'Invalid login credentials'
        //     ], 401);
        // }

        // $user = Auth::user();

        // $token = $user->createToken('authToken')->plainTextToken;

        // return response()->json([
        //     'response_code' => '200',
        //     'message' => 'Login successful | user berhasil login',
        //     //'access_token' => $token,
        //     'data' => [
        //         'token' => $token,
        //     ],
        // ]);
    }

    public function updatePassword(Request $request)
    {
        //$this->
        $request->validate([
            //'current_password' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:8',
        ]);

        // $user = Auth::user();

        // if (!Hash::check($request->current_password, $user->password)) {
        //     return response()->json([
        //         'message' => 'Invalid current password'
        //     ], 401);
        // }

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return response()->json([
                'response_code' => '00',
                'message' => 'User not found'
            ], 404);
        }

        $user->update([
            'password' => Hash::make($request->password),
        ]);

        return response()->json([
            'response_code' => '200',
            'message' => 'Password updated successfully | password  berhasil di update'
        ]);
    }

    // public function logout()
    // {
    //     auth()->logout();

    //     return response()->json(['message' => 'berhasil logged out']);
    // }

    public function logout(Request $request)
    {
        auth()->logout();
        // Auth::user()->tokens()->delete();

        return response()->json([
            'response_code' => '200',
            'message' => 'Logout successful | Logout Berhasil'
        ]);

    }

    public function me()
    {
        return response()->json(auth()->user());
    }

    public function getProfile(Request $request)
    {
        $user = Auth::user();

        return response()->json([
            'data' => $user,
        ]);
    }

    public function updateProfile(Request $request)
    {
        $user = Auth::user();

        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $user->id,
            'photo_profile' => 'image|mimes:jpg,jpeg,png|max:2048',
        ]);

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);

        if ($request->hasFile('photo_profile')) {
            $photo = $request->file('photo_profile');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $photo->storeAs('public/photo-profile', $filename);
            $user->update([
                'photo_profile' => $filename,
            ]);
        }

        return response()->json([
            'response_code' => '00',
            'message' => 'Profile updated successfully',
            'data' => $user,
        ], 200);
    }

    public function generateOtp(Request $request)
    {
        $request->validate([
            'email' => 'email|required',
        ]);

        $user = User::where('email', $request->email)->first();

        $user->generate_otp_code();

        $data['user']=$user;

        return response()->json([
            'response_code' => '00',
            'message' => 'OTP CODE Berhasil di Generate',
            'data' => $data,
        ], 200);

        $data['otp'] = $otp_code;

        event(new UserRegisterEvent($otp_code));
    }

    public function verifikasi(Request $request)
    {
        $request->validate([
            'otp' => 'required',
        ]);

        $otp_code = OtpCode::where('otp', $request->otp)->first();
        
        if(!$otp_code){
            return response()->json([
                'response_code' => '01',
                'message' => 'OTP CODE tidak di temukan'
            ], 400);
        }

        $now = Carbon::now();
        if($now > $otp_code->valid_until){
            return response()->json([
                'response_code' => '01',
                'message' => 'OTP CODE sudah tidak berlaku, silahkan generate ulang'
            ], 400);
        }

        //update user
        $user = User::find($otp_code->user_id);
        $user->email_verified_at = $now;
        $user->save();

        //delete OTP Code
        $otp_code->delete();

        return response()->json([
            'response_code' => '00',
            'message' => 'Email berhasil di verifikasi'
        ], 200);
    }
}