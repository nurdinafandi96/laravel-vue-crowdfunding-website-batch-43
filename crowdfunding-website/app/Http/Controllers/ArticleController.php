<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;
use File;


class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['article'] = Article::all();

        if(count($data['article']) === 0){
            return response()->json([
                'response_code' => '00',
                'response_massage' => 'data article campaign masih kosong',
            ], 200);
        }else{
            return response()->json([
                'response_code' => '00',
                'response_massage' => 'data article campaign berhasil ditampilkan',
                'data' => $data,
            ], 200);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     //
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'address' => 'required',
            'image' => 'mimes:jpg,png,jpeg',
            'required' => 'required',
            'collected' => 'required',
        ]);

        $article = new Article;

        $article->title = $request->title;
        $article->description = $request->description;
        $article->address = $request->address;
        $article->required = $request->required;
        $article->collected = $request->collected;

        if($request->hasFile('image')){
            $image = $request->file('image');
            //format file
            $image_extention = $image->getClientOriginalExtension();
            //name unique file
            $image_name = time(). '.'. $image_extention;
            //lokasi penyimpanan gambar
            $image_folder = '/photo-profile/article/';
            $image_location = $image_folder . $image_name;

            try {
                $image->move(public_path($image_folder), $image_name);

                $article->image = $image_location;
            } catch (\Throwable $th) {
                return response()->json([
                    'response_code' => '01',
                    'response_massage' => 'Image gagal di Upload',
                    'token' => $token,
                ], 400);
            }
        }

        $article->save();

        return response()->json([
            'response_code' => '00',
            'response_massage' => 'Tambah data campaign article berhasil',
            'token' => $token,
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Article::find($id);

        return response()->json([
            'response_code' => '00',
            'response_massage' => 'Detail data article campaign berhasil ditampilkan',
            'data' => $data,
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     //
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'address' => 'required',
            'image' => 'mimes:jpg,png,jpeg',
            'required' => 'required',
            'collected' => 'required',
        ]);

        $article = Article::find($id);

        $article->title = $request->title;
        $article->description = $request->description;
        $article->address = $request->address;
        $article->required = $request->required;
        $article->collected = $request->collected;

        if($request->hasFile('image')){
            $image = $request->file('image');
            //format file
            $image_extention = $image->getClientOriginalExtension();
            //name unique file
            $image_name = time(). '.'. $image_extention;
            //lokasi penyimpanan gambar
            $image_folder = '/photo-profile/article/';

            $subArticle = substr($article->$image, 1);

            File::delete($subArticle);
            $image_location = $image_folder . $image_name;

            try {
                $image->move(public_path($image_folder), $image_name);

                $article->image = $image_location;
            } catch (\Throwable $th) {
                return response()->json([
                    'response_code' => '01',
                    'response_massage' => 'Image gagal di Upload',
                    'token' => $token,
                ], 400);
            }
        }

        $article->save();

        return response()->json([
            'response_code' => '00',
            'response_massage' => 'update data article campaign berhasil',
            'token' => $token,
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::find($id);

        $subArticle = substr($article->$image, 1);

        File::delete($subArticle);

        $article->delete();

        return response()->json([
            'response_code' => '00',
            'response_massage' => 'delete data article campaign berhasil',
        ], 200);
    }
}
