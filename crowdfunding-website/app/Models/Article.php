<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class Article extends Model
{
    use HasFactory, UsesUuid;

    protected $fillable = ['title', 'description', 'address', 'image', 'image', 'required', 'collected'];
}
