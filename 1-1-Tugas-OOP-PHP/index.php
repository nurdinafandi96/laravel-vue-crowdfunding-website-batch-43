<?php

trait Hewan{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;
    
    public function atraksi() {
        echo $this->nama . ' sedang ' . $this->keahlian . '<br>';
    }
}

abstract class Fight{
    use Hewan;
    public $attackPower;
    public $defencePower;
    
    public function serang($lawan) {
        echo $this->nama . ' sedang menyerang ' . $lawan->nama . '<br>';
        $lawan->diserang($this);
    }
    
    public function diserang($lawan) {
        echo $this->nama . ' sedang diserang<br>';
        $this->darah -= ceil($lawan->attackPower / $this->defencePower * 0.5);
    }
}

class Elang extends Fight{
    public function __construct($nama){
        $this->nama = $nama;
        $this->jumlahKaki = 2;
        $this->keahlian = 'terbang tinggi';
        $this->attackPower = 10;
        $this->defencePower = 5;
    }
    
    public function getInfoHewan() {
        echo 'Jenis hewan: Elang <br>';
        echo 'Nama: ' . $this->nama . '<br>';
        echo 'Darah: ' . $this->darah . '<br>';
        echo 'Jumlah kaki: ' . $this->jumlahKaki . '<br>';
        echo 'Keahlian: ' . $this->keahlian . '<br>';
        echo 'Attack Power: ' . $this->attackPower . '<br>';
        echo 'Defence Power: ' . $this->defencePower . '<br>';
    }
}

class Harimau extends Fight{
    public function __construct($nama){
        $this->nama = $nama;
        $this->jumlahKaki = 4;
        $this->keahlian = 'lari cepat';
        $this->attackPower = 7;
        $this->defencePower = 8;
    }
    
    public function getInfoHewan() {
        echo 'Jenis hewan: Harimau <br>';
        echo 'Nama: ' . $this->nama . '<br>';
        echo 'Darah: ' . $this->darah . '<br>';
        echo 'Jumlah kaki: ' . $this->jumlahKaki . '<br>';
        echo 'Keahlian: ' . $this->keahlian . '<br>';
        echo 'Attack Power: ' . $this->attackPower . '<br>';
        echo 'Defence Power: ' . $this->defencePower . '<br>';
    }
}

$elang1 = new Elang('Elang 1');
$harimau1 = new Harimau('Harimau 1');

$elang1->getInfoHewan();
$elang1->atraksi();
$harimau1->getInfoHewan();
$harimau1->atraksi();

$elang1->serang($harimau1);
$harimau1->serang($elang1);

$elang1->getInfoHewan();
$harimau1->getInfoHewan();

?>
