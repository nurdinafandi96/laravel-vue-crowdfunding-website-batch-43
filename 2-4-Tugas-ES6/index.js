// Soal 1

const daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

daftarHewan.sort();

for(let i = 0; i < daftarHewan.length; i++) {
console.log(daftarHewan[i]);
}

// Soal 2

const ages = [23, 44, 26, 30];

const calcAverage = (arr) => {
  let sum = 0;
  for (let i = 0; i < arr.length; i++) {
    sum += arr[i];
  }
  return sum / arr.length;
};

const agesAverage = calcAverage(...ages);
console.log(`The average age is ${agesAverage}`);

// function introduce(data) {
// return Nama saya ${data.name}, umur saya ${data.age} tahun, alamat saya di ${data.address}, dan saya punya hobby yaitu ${data.hobby}!;
// }

// const data = {name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming"};

// const perkenalan = introduce(data);
// console.log(perkenalan);

// Soal 3

function hitung_huruf_vokal(string) {
const hurufVokal = 'aiueoAIUEO';
let jumlahVokal = 0;

for(let i = 0; i < string.length; i++) {
if(hurufVokal.indexOf(string[i]) !== -1) {
jumlahVokal++;
}
}

return jumlahVokal;
}

const hitung_1 = hitung_huruf_vokal("Muhammad");
const hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1, hitung_2);

// Soal 4

function naikAngkot(arrPenumpang) {
const rute = ['A', 'B', 'C', 'D', 'E', 'F'];
const harga = 2000;
const result = [];

for(let i = 0; i < arrPenumpang.length; i++) {
const penumpang = {
penumpang: arrPenumpang[i][0],
naikDari: arrPenumpang[i][1],
tujuan: arrPenumpang[i][2],
bayar: 0
};

const jarak = rute.indexOf(penumpang.tujuan) - rute.indexOf(penumpang.naikDari);
penumpang.bayar = jarak * harga;

result.push(penumpang);
}

return result;
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));

// ES6

// Soal 1

const luasDanKelilingPersegiPanjang = (panjang, lebar) => {
    const luas = panjang * lebar;
    const keliling = 2 * (panjang + lebar);
    return `Luas: ${luas}, Keliling: ${keliling}`;
  };
  
  console.log(luasDanKelilingPersegiPanjang(5, 8));
  
  // Soal 2
  
  const newFunction = (firstName, lastName) => {
    return {
      firstName,
      lastName,
      fullName() {
        console.log(`${firstName} ${lastName}`);
      },
    };
  };
  
  newFunction("William", "Imoh").fullName();
  

// Soal 3

const newObject = {
firstName: "Muhammad",
lastName: "Iqbal Mubarok",
address: "Jalan Ranamanyar",
hobby: "playing football",
};

const {firstName, lastName, address, hobby} = newObject;
console.log(firstName, lastName, address, hobby);

// soal 4

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east]; // menggunakan spread operator

console.log(combined); // ["Will", "Chris", "Sam", "Holly", "Gill", "Brian", "Noel", "Maggie"]

// soal 5

const planet = "earth";
const view = "glass";
const before = `Lorem ${view}dolor sit amet, consectetur adipiscing elit, ${planet}`;
